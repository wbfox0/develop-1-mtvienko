﻿using System;

namespace Calculator
{
    class Program
    {
        static void Main(string[] args)
        {
            bool op = Operate(3, 0, 3);

            Console.ReadKey();
        }

        static bool Operate(int a, int b, int op)
        {
            switch (op)
            {
                case 0:
                    Console.WriteLine($"Operation complete successful. Result of {a} + {b} is {a + b}");
                    return true;

                case 1:
                    Console.WriteLine($"Operation complete successful. Result of {a} - {b} is {a - b}");
                    return true;

                case 2:
                    Console.WriteLine($"Operation complete successful. Result of {a} * {b} is {a * b}");
                    return true;

                case 3:
                    if (op == 3 && b == 0)
                    {
                        Console.WriteLine("Impossible operation!");
                        return false;
                    }
                            
                    Console.WriteLine($"Operation complete successful. Result of {a} / {b} is {a / b}");
                    return true;

                default:
                    Console.WriteLine("There is no this operation!");
                    return false;
            }
        }
    }
}