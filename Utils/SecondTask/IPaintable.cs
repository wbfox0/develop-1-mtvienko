﻿namespace SecondTask
{
    public interface IPaintable
    {
        void Paint();
    }
}
