﻿namespace SecondTask
{
    internal abstract class Figure
    {
        public abstract int GetDimension();

        public abstract string GetFigureType();
    }
}
