﻿using SecondTask;

class main
{
    static void Main(string[] args)
    {
        Point dot1 = new Point("Dot1", 1);
        Point dot2 = new Point("Dot2", 2, 5);
        Point dot3 = new Point("Dot3", 3, 6, 0);
        Point dot4 = new Point("Dot4", 4, 7, 8);

        Console.WriteLine(Point.GetInfo(dot1));
        Console.WriteLine(Point.GetInfo(dot2, true));
        Console.WriteLine(Point.GetInfo(dot3, false));

        Console.WriteLine(Point.GetNameWithCoordinates(dot1));
        Console.WriteLine(Point.GetNameWithCoordinates(dot2));
        Console.WriteLine(Point.GetNameWithCoordinates(dot3));

        dot4.Paint();

        Console.ReadKey();
    }
}