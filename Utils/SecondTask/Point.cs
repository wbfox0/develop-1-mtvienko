﻿namespace SecondTask
{
    class Point : Figure, IPaintable
    {
        public string Name { get; }
        public double X { get; }
        public double Y { get; }
        public double Z { get; }
        public int Dim { get; }

        public Point(string name, double x)
        {
            Name = name;
            X = x;
            Dim = 1;
        }

        public Point(string name, double x, double y)
        {
            Name = name;
            X = x;
            Y = y;
            Dim = 2;
        }

        public Point(string name, double x, double y, double z)
        {
            Name = name;
            X = x;
            Y = y;
            Z = z;
            Dim = 3;
        }

        public static string GetInfo(Point point, bool ShowName = false) 
            => ShowName ? $"Point coodrinate {point.Name}: {point.X}." : $"Point coodrinate: {point.X}.";

        public static string GetNameWithCoordinates(Point point) => $"Point coodrinate {point.Name}: Dimension: {point.Dim}.";

        public override int GetDimension() => Dim;

        public override string GetFigureType() => "Point";

        public void Paint()
        {
            switch (Dim)
            {
                case 1:
                    Console.WriteLine($"Point Coordinates {Name}: {X}.");
                    break;

                case 2:
                    Console.WriteLine($"Point Coordinates {Name}: {X}, {Y}.");
                    break;

                case 3:
                    Console.WriteLine($"Point Coordinates {Name}: {X}, {Y}, {Y}.");
                    break;

                default:
                    Console.WriteLine("Error! Not enough data!");
                    break;
            }
        }
    }
}
